// #include <__clang_cuda_runtime_wrapper.h>
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>

#define size 20
#define factor_size 2
#define loop 10

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

__global__ void test(int (*corr_factor)[size+2], int *output) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  for (int i = 0; i < 5; i++) {
    __syncthreads();
    int chunk_size = 1 << i;
    int tmp = pos / chunk_size;
    int begin = tmp * chunk_size;
    int k = pos;
    if (tmp % 2 == 1 && pos < size) {
    if(pos == 4)
       printf("%d \n",output[pos]);
      for (int l = 0; l < min(factor_size, chunk_size); l++) {
        if (k - begin < 0 || begin - l - 1 < 0)
          break;

        output[k] += corr_factor[l][k - begin] * output[begin - l - 1];
      }
      if(pos == 4)
          printf("%d \n",output[pos]);

    }
    __syncthreads();
  }
}

int intput[size];
int output[size];
int verify[size];
int factor[factor_size] = {2, 1};
int corr_factor[factor_size][size+2];

int main(int argc, char const *argv[]) {
  for (int i = 0; i < size; i++) {
    intput[i] = output[i] = verify[i] = i;
  }
  corr_factor[0][0] = 0, corr_factor[0][1] = 1;
  corr_factor[1][0] = 1, corr_factor[1][1] = 0;

  for (int i = 2; i < size+2; i++) {
    for (int j = 0; j < factor_size; j++) {
      corr_factor[0][i] += factor[j] * corr_factor[0][i - j - 1];
      corr_factor[1][i] += factor[j] * corr_factor[1][i - j - 1];
    }
  }
  for (int i = 0; i < size; i++) {
    corr_factor[0][i] = corr_factor[0][i + 2];
    corr_factor[1][i] = corr_factor[1][i + 2];
  }

  for (int i = 1; i < size; i++) {
    for (int j = 0; j < factor_size; j++) {
      if (i - j - 1 > 0)
        verify[i] += factor[j] * verify[i - j - 1];
    }
  }
  //  parallelize this part
  int *dev_output, (*dev_corr_factor)[size+2];
  cudaMalloc(&dev_output, size * sizeof(int));
  cudaMalloc(&dev_corr_factor, size * 2 * sizeof(int));
  cudaMemcpy(dev_output, output, size * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(dev_corr_factor, corr_factor, 2 * size * sizeof(int),
             cudaMemcpyHostToDevice);
  test<<<1, 20>>>(dev_corr_factor, dev_output);
  cudaMemcpy(output, dev_output, size * sizeof(int), cudaMemcpyDeviceToHost);

      for (int i=0; i < size; i++) {
          printf("%d ", output[i]);
      }
      printf("\n");
      for (int i=0; i < size; i++) {
          printf("%d ", verify[i]);
      }
      printf("\n");

  // printf("\n");
  cudaFree(dev_output);
  cudaFree(dev_corr_factor);

      for (int i = 0; i < size; i++)
      {
          if (verify[i] != output[i])
              printf("%d different\n", i);
      }
  printf("hello");
  return 0;
}
