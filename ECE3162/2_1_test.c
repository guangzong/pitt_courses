#include <stdio.h>

#define size        20
#define factor_size 2
#define loop        20

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

// for (int i = 0; i < loop; i++) {
//     int chunk_size = 1 << i;
//     for (int j = 0; j < size; j += chunk_size) {
//         j += chunk_size;
//         //  (2n -  1 ) * chunk_size  < k < (2n) * chunk_size;
//         //  k < size
//         // j = (2n-1) * chunk_size
//         for (int k = j; k < j + chunk_size; k++) {
//             if (k > size)
//                 break;
//             for (int l = 0; l < min(factor_size, chunk_size); l++) {
//                 output[k] += corr_factor[l][k - j] * output[j - l - 1];
//             }
//         }
//     }
// }

// __global__ void test(int i, int **corr_factor, int ){
//     int pos = blockIdx.y * blcokDim.x + blockIdx.x;
//     int chunk_size = 1 << i;
//     if

//     __syncthreads();

// }

int intput[size];
int output[size];
int verify[size];
int factor[factor_size] = {2, 1};
int corr_factor[factor_size][size];

int main(int argc, char const *argv[]) {

    // intput[0] = output[0] = verify[0] = 0;
    for (int i = 0; i <= size; i++) {
        intput[i] = output[i] = verify[i] = i + 1;
    }
    corr_factor[0][0] = 0, corr_factor[0][1] = 1;
    corr_factor[1][0] = 1, corr_factor[1][1] = 0;

    for (int i = 2; i <= size; i++) {
        for (int j = 0; j < factor_size; j++) {
            corr_factor[0][i] += factor[j] * corr_factor[0][i - j - 1];
            corr_factor[1][i] += factor[j] * corr_factor[1][i - j - 1];
        }
    }
    for (int i = 0; i < size; i++) {
        corr_factor[0][i] = corr_factor[0][i + 2];
        corr_factor[1][i] = corr_factor[1][i + 2];
    }
    for (int i = 0; i < size; i++) {
        printf("%d ", corr_factor[0][i]);
    }
    printf("\n");
    for (int i = 0; i < size; i++) {
        printf("%d ", corr_factor[1][i]);
    }
    printf("\n");

    for (int i = 1; i < size; i++) {
        for (int j = 0; j < factor_size; j++) {
            if (i - j - 1 >= 0)
                verify[i] += factor[j] * verify[i - j - 1];
        }
    }
    //  parallelize this part

    for (int i = 0; i < loop; i++) {
        int chunk_size = 1 << i;
        for (int n = 1; n < size / (2 * chunk_size) + 2; n++) {
            int begin = (2 * n - 1) * chunk_size;
            int end   = (2 * n) * chunk_size;
            for (int k = begin; k < end; k++) {
                if (k > size)
                    break;
                int j = (2 * n - 1) * chunk_size;
                for (int l = 0; l < min(factor_size, chunk_size); l++) {
                    output[k] += corr_factor[l][k - j] * output[j - l - 1];
                }
            }
        }
        for (int lcnt = 0; lcnt < size; lcnt++) {
            printf("%d ", output[lcnt]);
        }
        printf("\n");
    }

    for (int i = 0; i < size; i++) {
        if (verify[i] != output[i])
            printf("%d diff\n", i);
    }

    for (int i = 0; i < size; i++) {
        printf("%d ", verify[i]);
    }
    printf("\n");

    for (int i = 0; i < size; i++) {
        printf("%d ", output[i]);
    }
    printf("\n");
    return 0;
}
