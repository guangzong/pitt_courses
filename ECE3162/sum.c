#define size 100000

#include <stdio.h>

int intput[size];
int output[size];
int verify[size];
int factor[1]   = {1};
int factor_size = 1;
int corr_factor[1][size];

int main(int argc, char const *argv[]) {
    for (int i = 0; i < size; i++) {
        intput[i] = output[i] = verify[i] = i;
    }
    for (int i = 0; i < 20; i++) {
        int chunk_size = 1<<i;
        for (int j = 0; j < size; j += chunk_size) {
            j += chunk_size;
            for (int k = j; k < j + chunk_size; k++) {
                if (k > size)
                    break;
                output[k] += output[j - 1];
            }
        }
    }
    verify[0] = 0;
    for (int i = 1; i < size; i++) {
        verify[i] += verify[i - 1];
    } 

    for (int i = 0; i < size; i++) {
        if (verify[i] != output[i])
            printf("%d diff\n",i);
    }
    return 0;
}
