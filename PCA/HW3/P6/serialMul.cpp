#include <iostream>
#include <bits/stdc++.h>
#define max_value int(1e5)
using namespace std;
int main(int argc, char** argv){
	freopen("data.in","r",stdin);
	// freopen("data.out","w",stdout);
	int M,N,P;
	cin>>M>>N>>P;
	cout<<M << N << P<<endl;
	vector<int> A[max_value];
	vector<int> B[max_value];
	vector<int> C[max_value];
	for(int i=0;i<M;i++){
		for(int j=0;j<N;j++)	{
			int tmp;cin>>tmp;
			A[i].push_back(tmp);
		}
	}
	for(int i=0;i<N;i++){
		for(int j=0;j<P;j++)	{
			int tmp;cin>>tmp;
			B[i].push_back(tmp);
		}
	}
	for(int i=0;i<M;i++){
		for(int j=0;j<P;j++){
			int tmp = 0 ;
			for(int k=0;k<N;k++){
				tmp += A[i][k]*B[k][j];
			}
			C[i].push_back(tmp);
		}
	}
	//verify
	bool flag =  true;
	for(int i=0;i<M;i++){
		for(int j=0;j<P;j++){
			int tmp;cin>>tmp;
			if(C[i][j]!= tmp)
				flag = false;
			// cout<<"i:"<<i<<" j:"<<j<<" C[i][j]:"<< C[i][j]<<" tmp:"<<tmp<<endl;
		}
	}
	if(flag)
		cout<<"yes"<<endl;
	else
		cout<<"NO"<<endl;
}

