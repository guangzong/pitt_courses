#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define M 13
#define N 347
#define P 23
int main() {
	int* a =(int*) malloc(sizeof(int) * M * N);
	int* b =(int *) malloc(sizeof(int) * N * P);
	int* c = (int*)malloc(sizeof(int) * M * P);
	for (int i = 0; i < M * N; i++)
	{
		a[i] = i;
		/* printf("%d ", a[i]); */
	}
	printf("\n");
	for (int i = 0; i < N * P; i++)
	{
		b[i] = i;
		/* printf("%d ", b[i]); */
	}
	/* printf("\n"); */
	double t1,t2;
	t1 = MPI_Wtime();
	for (int i = 0; i < M * P; i++) {
		c[i] = 0;
		// row: i/P column: i%p
		/* printf("%d ", i / P); */
		/* printf("%d \n", i % P); */
		for (int j = 0; j < N; j++) {
			c[i] += a[i / P * N + j] * b[i % P + j * P];
		}
		//printf("%d ", c[i]);
	}
	t2 = MPI_Wtime();
	printf("serial time elapse: %0.9lf",t2-t1);
}
