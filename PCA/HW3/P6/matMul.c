#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define gatherInt(send, count, rev) \
	MPI_Gather(send, count, MPI_INT, rev, count, MPI_INT, 0, MPI_COMM_WORLD)
#define scattervInt(send, sendCounts, displs, rev, revCounts)                \
	MPI_Scatterv(send, sendCounts, displs, MPI_INT, rev, revCounts, MPI_INT, \
			0, MPI_COMM_WORLD)
#define bcast(send, sendCount) \
	MPI_Bcast(send, sendCount, MPI_INT, 0, MPI_COMM_WORLD)
#define forn(i, n) for (i = 0; i < n; i++)
#define printInt(num) printf("%d ", num)

#define M 13
#define N 347
#define P 23

int main(int argc, char *argv[]) {
	int cores =  atoi(argv[1]);
	MPI_Init(&argc, &argv);
	int rank, worldSize;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
	int i,j;
	int *A, *B, *C;
	A = malloc(sizeof(int) * M * N);
	B = malloc(sizeof(int) * N * P);
	if (rank == 0) {
		// generate data at master node
		forn(i, M) forn(j, N) A[i * N + j] = (i * N + j)%10; // matrix A M * N
		forn(i, N) forn(j, P) B[i * P + j] = (i * P + j)%10; // matrix B N * P
	}

	double t1 = MPI_Wtime();
	double t2;
	int numberOfLineEachNode = M / worldSize;
	if (M % worldSize != 0)
		numberOfLineEachNode++;
	if (rank == 0)
		C = malloc(sizeof(int) * numberOfLineEachNode * worldSize * P);

	int displs[worldSize];
	forn(i, worldSize) { displs[i] = min(N * M, i * N * numberOfLineEachNode); }
	int *sendCounts = malloc(sizeof(int) * worldSize);
	forn(i, worldSize - 1) sendCounts[i] = displs[i + 1] - displs[i];
	sendCounts[worldSize - 1] = M * N - displs[worldSize - 1];
	// forn(i, worldSize) printInt(sendCounts[i])	;
	int *miniA = malloc(sizeof(int) * N * numberOfLineEachNode);
	scattervInt(A, sendCounts, displs, miniA, sendCounts[rank]);
	bcast(B, N * P);
	int *ans = malloc(sizeof(int) * numberOfLineEachNode * P);
#pragma omp parallel num_threads(cores) default(none) shared(ans,miniA,B,numberOfLineEachNode) private(i,j)
	{
#pragma omp for
		forn(i, numberOfLineEachNode * P) {
			ans[i] = 0;
			// calculate the node value node(n,p) X = i/P, Y = i%P
			// miniA: numberofLineEachNode * N             B: N * P       ans:
			// numberofLineaEachNode*P
			forn(j, N) { ans[i] += miniA[i / P * N + j] * B[(i % P) + j * P]; }
			// ans[i] = nodeValue;
		}
	}
	gatherInt(ans, numberOfLineEachNode * P, C);
	if (rank ==0)
		t2 = MPI_Wtime();
	if(rank ==0)
		printf("time elapse: %lf",MPI_Wtime()-t1);
	free(A), free(B), free(miniA);
	free(ans);
	if (rank == 0)
		free(C);
	free(sendCounts);
	MPI_Finalize();
	return 0;
}
