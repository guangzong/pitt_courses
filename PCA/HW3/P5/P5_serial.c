#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define M 13
#define N 347
#define P 23
#define forn(i, n) for (int i = 0; i < n; i++)

int main(int argc, char *argv[]) {

	int **A, **B,i,j,k;
	int **C_serial;
	// allocae space
	A = (int **)malloc(sizeof(int *) * M);
	B = (int **)malloc(sizeof(int *) * N);
	C_serial = (int **)malloc(sizeof(int *) * M);

	forn(i, M) A[i] = malloc(sizeof(int) * N);
	forn(i, N) B[i] = malloc(sizeof(int) * P);
	forn(i, M) C_serial[i] = malloc(sizeof(int) * P);
	/* forn(i, M) forn(j, N) A[i][j] = (i + j) % 10; */
	/* forn(i, N) forn(j, P) B[i][j] = (i + j) % 10; */

	forn(i, M) forn(j, N) A[i][j] = 1;
	forn(i, N) forn(j, P) B[i][j] = 1;

	double t1, t2, t3, para_elapse, serial_elapse;
	t1 = omp_get_wtime();
	t2 = omp_get_wtime();
	forn(i, M) forn(j, P) forn(k, N) C_serial[i][j] += A[i][k] * B[k][j];
	t3 = omp_get_wtime();
	para_elapse = t2 - t1;
	serial_elapse = t3 - t2;
	printf("serial algorithm time elapse:   %0.9lf\n", serial_elapse);

	forn(i, M) free(A[i]), free(C_serial[i]);
	forn(i, N) free(B[i]);
	free(A), free(B), free(C_serial);
}
