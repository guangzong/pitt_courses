#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define M 13
#define N 347
#define P 23
#define forn(i, n) for (int i = 0; i < n; i++)

void printMatrix(int **A, int **B, int **C) {
	forn(i, M) {
		forn(j, N) printf("%d ", A[i][j]);
		printf("\n");
	}
	forn(i, N) {
		forn(j, P) printf("%d ", B[i][j]);
		printf("\n");
	}
	forn(i, M) {
		forn(j, P) printf("%d ", C[i][j]);
		printf("\n");
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2)
		printf("the number of arguments expect one\n");
	int processor_number = atoi(argv[1]);
	printf("the number of processor: %d\n", processor_number);
	int num_procs = omp_get_num_procs();
	if (processor_number > num_procs) {
		printf("error, no enough processor\n");
	}

	int **A, **B, **C,i,j,k;
	int **C_serial;
	// allocae space
	A = (int **)malloc(sizeof(int *) * M);
	B = (int **)malloc(sizeof(int *) * N);
	C = (int **)malloc(sizeof(int *) * M);
	C_serial = (int **)malloc(sizeof(int *) * M);

	forn(i, M) A[i] = malloc(sizeof(int) * N);
	forn(i, N) B[i] = malloc(sizeof(int) * P);
	forn(i, M) C[i] = malloc(sizeof(int) * P);
	forn(i, M) C_serial[i] = malloc(sizeof(int) * P);
	/* forn(i, M) forn(j, N) A[i][j] = (i + j) % 10; */
	/* forn(i, N) forn(j, P) B[i][j] = (i + j) % 10; */

	forn(i, M) forn(j, N) A[i][j] = 1;
	forn(i, N) forn(j, P) B[i][j] = 1;

	double t1, t2, t3, para_elapse, serial_elapse;
	t1 = omp_get_wtime();
#pragma omp parallel default(none) num_threads(processor_number) shared(A,B,C) private(i, j, k) 
	{
#pragma omp for 
		for (i = 0; i < M; i++) {
			for (j = 0; j < P; j++) {
				C[i][j] = 0;
				for (k = 0; k < N; k++) { C[i][j] += A[i][k] * B[k][j]; }
			}
		}
	}

    t2 = omp_get_wtime();
    forn(i, M) forn(j, P) forn(k, N) C_serial[i][j] += A[i][k] * B[k][j];
	t3 = omp_get_wtime();

	para_elapse = t2 - t1;
	serial_elapse = t3 - t2;
	forn(i, M) forn(j, P) if (C[i][j] != C_serial[i][j]) printf("error");
	printf("parallel algorithm time elapse: %0.9lf\n", para_elapse);
	printf("serial algorithm time elapse:   %0.9lf\n", serial_elapse);
	printf("parallel efficiency: %lf \n",
		   serial_elapse / (processor_number * para_elapse));
	printf("speed up: %0.9lf:\n",serial_elapse/para_elapse);
	printf("\n");

	forn(i, M) free(A[i]), free(C[i]), free(C_serial[i]);
	forn(i, N) free(B[i]);
	free(A), free(B), free(C), free(C_serial);
}
