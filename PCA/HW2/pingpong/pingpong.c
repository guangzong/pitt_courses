#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define gatherInt(send, count, rev) \
	MPI_Gather(send, count, MPI_INT, rev, count, MPI_INT, 0, MPI_COMM_WORLD)
#define gatherFloat(send, rev) \
	MPI_Gather(send, 1, MPI_FLOAT, rev, 1, MPI_FLOAT, 0, MPI_COMM_WORLD)
#define gatherChar(send, rev) \
	MPI_Gather(send, 1, MPI_CHAR, rev, 1, MPI_CHAR, 0, MPI_COMM_WORLD)

#define scattervInt(send, sendCounts, displs, rev, revCounts)                \
	MPI_Scatterv(send, sendCounts, displs, MPI_INT, rev, revCounts, MPI_INT, \
			0, MPI_COMM_WORLD)
#define bcast(send, sendCount) \
	MPI_Bcast(send, sendCount, MPI_INT, 0, MPI_COMM_WORLD)
#define forn(i, n) for(int i = 0; i < n;i++)
#define printInt(num) printf("%d ", num)

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int rank, world_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	int max_size = 1e9;
	char *data = malloc(sizeof(char) * max_size);
	forn(i, max_size) data[i] = 'h';

	double t1 = MPI_Wtime();
	double t2 = 0;
	// printf("hello");
	for (int size = 10000000; size < max_size; size += 10000000) {
		MPI_Barrier(MPI_COMM_WORLD);
		double t1 = MPI_Wtime();
		if (rank == 0) {
			MPI_Send(data, size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
			MPI_Recv(data, size, MPI_CHAR, 1, 0, MPI_COMM_WORLD,
					MPI_STATUS_IGNORE);
			t2 = MPI_Wtime();
			printInt(size);
			printf(" %lf \n", t2 - t1);
			//check data
			forn(i,size){
				if(data[i]!='h')
					printf("different");
			}
		}
		if (rank == 1) {
			MPI_Recv(data, size, MPI_CHAR, 0, 0, MPI_COMM_WORLD,
					MPI_STATUS_IGNORE);
			MPI_Send(data, size, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	free(data);
	MPI_Finalize();
	return 0;
}
