#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define gatherInt(send, count, rev) \
	MPI_Gather(send, count, MPI_INT, rev, count, MPI_INT, 0, MPI_COMM_WORLD)
#define gatherFloat(send, rev) \
	MPI_Gather(send, 1, MPI_FLOAT, rev, 1, MPI_FLOAT, 0, MPI_COMM_WORLD)
#define gatherChar(send, rev) \
	MPI_Gather(send, 1, MPI_CHAR, rev, 1, MPI_CHAR, 0, MPI_COMM_WORLD)

#define scattervInt(send, sendCounts, displs, rev, revCounts)                \
	MPI_Scatterv(send, sendCounts, displs, MPI_INT, rev, revCounts, MPI_INT, \
				 0, MPI_COMM_WORLD)
#define bcast(send, sendCount) \
	MPI_Bcast(send, sendCount, MPI_INT, 0, MPI_COMM_WORLD)
#define forn(i, n) for (int i = 0; i < n; i++)
#define printInt(num) printf("%d ", num)

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int rank, world_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Finalize();
	return 0;
}

