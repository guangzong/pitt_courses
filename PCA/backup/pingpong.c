
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define gatherInt(send, count, rev) \
	MPI_Gather(send, count, MPI_INT, rev, count, MPI_INT, 0, MPI_COMM_WORLD)
#define gatherFloat(send, rev) \
	MPI_Gather(send, 1, MPI_FLOAT, rev, 1, MPI_FLOAT, 0, MPI_COMM_WORLD)
#define gatherChar(send, rev) \
	MPI_Gather(send, 1, MPI_CHAR, rev, 1, MPI_CHAR, 0, MPI_COMM_WORLD)

#define scattervInt(send, sendCounts, displs, rev, revCounts)                \
	MPI_Scatterv(send, sendCounts, displs, MPI_INT, rev, revCounts, MPI_INT, \
				 0, MPI_COMM_WORLD)
#define bcast(send, sendCount) \
	MPI_Bcast(send, sendCount, MPI_INT, 0, MPI_COMM_WORLD)
#define forn(i, n) for (int i = 0; i < n; i++)
#define printInt(num) printf("%d ", num)

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int rank, world_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	int max_size = 1e9;
	char *data = malloc(sizeof(char) * max_size);
	forn(i, size) data[i] = 'h';

	// printf("hello");
	for (unsigned long long size = 1e7; size < max_size; size += 1e7) {
		MPI_Barrier(MPI_COMM_WORLD);
		double t1 = MPI_Wtime();
		double t2 = 0;
		if (rank == 0) {
			MPI_Send(data, size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
			MPI_Recv(data, size, MPI_CHAR, 1, 0, MPI_COMM_WORLD,
					 MPI_STATUS_IGNORE);
			printInt(size);
			printf(" %lf \n", MPI_Wtime() - t1);
		}
		if (rank == 1) {
			MPI_Recv(data, size, MPI_CHAR, 0, 0, MPI_COMM_WORLD,
					 MPI_STATUS_IGNORE);
			MPI_Send(data, size, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
		}
		free(data);
		MPI_Barrier(MPI_COMM_WORLD);
	}
	MPI_Finalize();
	return 0;
}
