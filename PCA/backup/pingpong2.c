#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define forn(i, n) for (int i = 0; i < n; i++)
#define printInt(num) printf("%d ", num)

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int rank, world_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	char *data = malloc(sizeof(char) * 1e4);
	char *revBuf = malloc(sizeof(char) * 1e4);
	forn(i, 1e4) data[i] = 'h';
	MPI_Request *request = malloc(sizeof(MPI_Request));
	int flag;
	for (int size = 1e3; size < 1e4; size += 1e3) {
		MPI_Barrier(MPI_COMM_WORLD);
		double t1 = MPI_Wtime();
		if (rank == 0) {
			MPI_Isend(data, size, MPI_CHAR, 1, 0, MPI_COMM_WORLD, request);
			MPI_Irecv(revBuf, size, MPI_CHAR, 1, 0, MPI_COMM_WORLD, request);
			flag = 0;
			while (!flag) MPI_Test(request, &flag, MPI_STATUS_IGNORE);
			printInt(size);
			printf(" %lf \n", MPI_Wtime() - t1);
		}
		if (rank == 1) {
			MPI_Irecv(revBuf, size, MPI_CHAR, 0, 0, MPI_COMM_WORLD, request);
			flag = 0;
			while (!flag) MPI_Test(request, &flag, MPI_STATUS_IGNORE);
			MPI_Isend(revBuf, size, MPI_CHAR, 0, 0, MPI_COMM_WORLD, request);				
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	free(data);
	free(revBuf);
	free(request);
	MPI_Finalize();
	return 0;
}
