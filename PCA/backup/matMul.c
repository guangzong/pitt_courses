#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define gatherInt(send, count, rev) \
	MPI_Gather(send, count, MPI_INT, rev, count, MPI_INT, 0, MPI_COMM_WORLD)
#define gatherFloat(send, rev) \
	MPI_Gather(send, 1, MPI_FLOAT, rev, 1, MPI_FLOAT, 0, MPI_COMM_WORLD)
#define gatherChar(send, rev) \
	MPI_Gather(send, 1, MPI_CHAR, rev, 1, MPI_CHAR, 0, MPI_COMM_WORLD)
#define scattervInt(send, sendCounts, displs, rev, revCounts)                \
	MPI_Scatterv(send, sendcounts, displs, MPI_INT, rev, revCounts, MPI_INT, \
				 0, MPI_COMM_WORLD)
#define bcast(send, sendCount) \
	MPI_Bcast(send, sendCount, MPI_INT, 0, MPI_COMM_WORLD)
#define forn(i, n) for (int i = 0; i < n; i++)
#define printInt(num) printf("%d ", num)

int main(int argc, char *argv[]) {

	MPI_Init(&argc, &argv);
	int rank, worldSize;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
	int M, N, P;
	M = 2, N = 2, P = 3;
	int *A, *B, *C;
	A = malloc(sizeof(int) * M * N);
	B = malloc(sizeof(int) * N * P);

	if (rank == 0) {
		// generate data at master node
		forn(i, M) forn(j, N) A[i * N + j] = i * N + j; // matrix A M * N
		forn(i, N) forn(j, P) B[i * P + j] = i * P + j; // matrix B N * P
	}
	bcast(A, M * N), bcast(B, N * P);
	MPI_Barrier(MPI_COMM_WORLD);
	// number of node in each processsor
	int node = M * P / worldSize;
	if (M * P % worldSize != 0)
		node++;

	if (rank == 0)
		C = malloc(sizeof(int) * node * worldSize);

	int *data = malloc(sizeof(int) * node);
	int upBound = min(node * (rank + 1), M * P);
	for (int i = rank * node; i < upBound; i++) {
		int nodeValue = 0;
		// calculate the node value node(n,p) X = i/P, Y = i%P
		// A: M * N             B: N * P       C: M*P
		forn(j, N) {
			nodeValue += A[i / P * N + j] * B[(i % P) + j * P];
		}
		data[i - rank * node] = nodeValue;
	}

	gatherInt(data, node, C);
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) {
		forn(i, M * P) {
			printInt(C[i]);
			if ((i+1) % P == 0)
				printf("\n");
		}
	}
	free(A);
	free(B);
	free(data);
	if (rank == 0)
		free(C);
	MPI_Finalize();
	return 0;
}