
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf


mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train, x_test = x_train / 255.0, x_test / 255.0
x_train = x_train.reshape((60000, 28, 28, 1))
y_train = y_train.reshape([60000, 1])
print(x_test.shape)
x_test = x_test.reshape((10000, 28, 28, 1))
y_test = y_test.reshape([10000, 1])


# torch.Size([19, 1, 5, 5])
# torch.Size([20, 19, 5, 5])
# torch.Size([3, 20, 1, 1])
# torch.Size([19, 3, 4, 4])
# torch.Size([20, 19, 3, 3])//4
# torch.Size([20, 20, 5, 5])//5
# torch.Size([8, 20, 1, 1])//61
# torch.Size([20, 8, 5, 5])//62
# torch.Size([10, 980])

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(19, (5, 5), activation='relu',
                           input_shape=(28, 28, 1), use_bias=False),
    tf.keras.layers.Conv2D(20, (5, 5), activation='relu', use_bias=False),
    tf.keras.layers.Conv2D(3, (1, 1), use_bias=False),
    tf.keras.layers.Conv2D(19, (4, 4), activation='relu', use_bias=False),
    tf.keras.layers.Conv2D(20, (3, 3), activation='relu', use_bias=False),
    tf.keras.layers.Conv2D(20, (5, 5), activation='relu', use_bias=False),
    tf.keras.layers.Conv2D(8, (1, 1), use_bias=False),
    tf.keras.layers.Conv2D(20, (5, 5), activation='relu', use_bias=False),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(10, activation='softmax', use_bias=False)

])
model.compile(optimizer='sgd',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(x_train, y_train, epochs=40)
# model.fit(x_train, y_train)
#  print(x_train[0].shape, y_train)
print(x_train.shape, y_train.shape)

model.evaluate(x_test,  y_test, verbose=2)

# Save the model
model.save('mnist.h5')

# Recreate the exact same model purely from the file
new_model = tf.keras.models.load_model('mnist.h5')
