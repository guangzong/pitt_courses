
import os
import json
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf


mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train, x_test = x_train / 255.0, x_test / 255.0
x_train = x_train.reshape((60000, 28, 28, 1))
y_train = y_train.reshape([60000, 1])
print(x_test.shape)
x_test = x_test.reshape((10000, 28, 28, 1))
y_test = y_test.reshape([10000, 1])

# class MyModel(tf.keras.Model):
#     def __init__(self):
#         super(MyModel, self).__init__()
#         self.Conv1 = tf.keras.layers.Conv2D(
#             20, (3, 3), activation='relu', input_shape=(28, 28, 1), use_bias=False, padding='same')
#         self.MaxPool = tf.keras.layers.MaxPool2D(pool_size=(2, 2))
#         self.Sep2d = tf.keras.layers.SeparableConv2D(
#             50, (5, 5), activation='relu', use_bias=False, padding='same')
#         self.Conv2 = tf.keras.layers.Conv2D(
#             50, (1, 1), activation='relu', use_bias=False)
#         self.dense = tf.keras.layers.Dense(
#             500, activation='relu', use_bias=False)
#         self.dense2 = tf.keras.layers.Dense(
#             10, activation='softmax', use_bias=False)
#         self.flatten = tf.keras.layers.Flatten()

#     def call(self, inputs):
#         x1 = self.Conv1(inputs)
#         x1 = self.MaxPool(x1)
#         x1 = self.Sep2d(x1)
#         x2 = self.MaxPool(inputs)
#         x2 = self.Conv2(x2)
#         x1 = x1+x2
#         x1 = self.MaxPool(x1)
#         x1 = self.flatten(x1)
#         x1 = self.dense(x1)
#         x1 = self.dense2(x1)
#         return x1

Conv1 = tf.keras.layers.Conv2D(20, (3, 3), activation='relu',input_shape=(28, 28, 1), use_bias=False, padding='same')
MaxPool = tf.keras.layers.MaxPool2D(pool_size=(2, 2))
MaxPool2 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))
Sep2d = tf.keras.layers.SeparableConv2D(
    50, (5, 5), activation='relu', use_bias=False, padding='same')

MaxPool3 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))
Add = tf.keras.layers.Add()
Conv2 = tf.keras.layers.Conv2D(50, (1, 1), activation='relu', use_bias=False)
dense = tf.keras.layers.Dense(500, activation='relu', use_bias=False)
dense2 = tf.keras.layers.Dense(10, activation='softmax', use_bias=False)
flatten = tf.keras.layers.Flatten()

inputs = tf.keras.Input(shape=(28, 28, 1))
x1 = Conv1(inputs)
x1 = MaxPool(x1)
x1 = Sep2d(x1)
x2 = MaxPool3(inputs)
x2 = Conv2(x2)
x1 = Add([x1 , x2])
x1 = MaxPool2(x1)
x1 = flatten(x1)
x1 = dense(x1)
outputs = dense2(x1)

model = tf.keras.Model(inputs=inputs, outputs=outputs)

# model = MyModel()


model.compile(optimizer='sgd',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(x_train, y_train, epochs=40)
# model.fit(x_train, y_train)
#  print(x_train[0].shape, y_train)
print(x_train.shape, y_train.shape)

model.evaluate(x_test,  y_test, verbose=2)
# model.to_json()
# tf.saved_model.save(model, './test')
# Save the model
# tf.saved_model.save(model,'./')
# model.save('./bonsai', save_format="tf")
# model.save_weights('Bonsai.h5')
# model.save('bonsai.h5')
# model.save_weights('bonsai.h5');

# Recreate the exact same model purely from the file
# new_model = tf.keras.models.load_model('Bonsai.h5')

model.save('Bonsai.h5')
# json_config = model.to_json()
# with open('Bonsai.json', 'w') as json_file:
#     json_file.write(json_config)
# # Save weights to disk
# model.save_weights('Bonsai.h5')

# # Reload the model from the 2 files we saved
# with open('model_config.json') as json_file:
#     json_config = json_file.read()
# new_model = tf.keras.models.model_from_json(json_config)
# new_model.load_weights('Bonsai.h5')
