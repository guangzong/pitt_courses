
#ifdef __cplusplus
 extern "C" {
#endif
/**
  ******************************************************************************
  * @file           : app_x-cube-ai.c
  * @brief          : AI program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "app_x-cube-ai.h"
#include "main.h"
//#include "constants_ai.h"
#include "ai_datatypes_defines.h"

/* USER CODE BEGIN includes */
/* USER CODE END includes */
/* USER CODE BEGIN initandrun */
#include <stdlib.h>

/* Global handle to reference the instance of the NN */
static ai_handle lesp = AI_HANDLE_NULL;
static ai_buffer ai_input[AI_LESP_IN_NUM] = AI_LESP_IN ;
static ai_buffer ai_output[AI_LESP_OUT_NUM] = AI_LESP_OUT ;

/*
 * Init function to create and initialize a NN.
 */
int aiInit(const ai_u8* activations)
{
    ai_error err;

    /* 1 - Specific AI data structure to provide the references of the
     * activation/working memory chunk and the weights/bias parameters */
    const ai_network_params params = {
            AI_LESP_DATA_WEIGHTS(ai_lesp_data_weights_get()),
            AI_LESP_DATA_ACTIVATIONS(activations)
    };

    /* 2 - Create an instance of the NN */
    err = ai_lesp_create(&lesp, AI_LESP_DATA_CONFIG);
    if (err.type != AI_ERROR_NONE) {
	    return -1;
    }

    /* 3 - Initialize the NN - Ready to be used */
    if (!ai_lesp_init(lesp, &params)) {
        err = ai_lesp_get_error(lesp);
        ai_lesp_destroy(lesp);
        lesp = AI_HANDLE_NULL;
	    return -2;
    }

    return 0;
}

/*
 * Run function to execute an inference.
 */
int aiRun(const void *in_data, void *out_data)
{
    ai_i32 nbatch;
    ai_error err;

    /* Parameters checking */
    if (!in_data || !out_data || !lesp)
        return -1;

    /* Initialize input/output buffer handlers */
    ai_input[0].n_batches = 1;
    ai_input[0].data = AI_HANDLE_PTR(in_data);
    ai_output[0].n_batches = 1;
    ai_output[0].data = AI_HANDLE_PTR(out_data);

    /* 2 - Perform the inference */
    nbatch = ai_lesp_run(lesp, &ai_input[0], &ai_output[0]);
    if (nbatch != 1) {
        err = ai_lesp_get_error(lesp);
        // ...
        return err.code;
    }

    return 0;
}
/* USER CODE END initandrun */

/*************************************************************************
  *
  */
void MX_X_CUBE_AI_Init(void)
{
    /* USER CODE BEGIN 0 */
    /* Activation/working buffer is allocated as a static memory chunk
     * (bss section) */
    AI_ALIGNED(4)
    static ai_u8 activations[AI_LESP_DATA_ACTIVATIONS_SIZE];

    aiInit(activations);
    /* USER CODE END 0 */
}


void MX_X_CUBE_AI_Process(void)
{
    /* USER CODE BEGIN 1 */


    /* Example of definition of the buffers to store the tensor input/output */
    /*  type is dependent of the expected format                             */
//    AI_ALIGNED(4)
    static ai_i8 in_data[AI_LESP_IN_1_SIZE_BYTES];

//    AI_ALIGNED(4)
    static ai_i8 out_data[AI_LESP_OUT_1_SIZE_BYTES];

    /* Retrieve format/type of the first input tensor - index 0 */
    const ai_buffer_format fmt_ = AI_BUFFER_FORMAT(&ai_input[0]);
    const uint32_t type_ = AI_BUFFER_FMT_GET_TYPE(fmt_);
    ai_float val[] ={
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.329412, 0.725490,
                        0.623529, 0.592157, 0.235294, 0.141176, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.870588, 0.996078, 0.996078, 0.996078,
                        0.996078, 0.945098, 0.776471, 0.776471, 0.776471, 0.776471,
                        0.776471, 0.776471, 0.776471, 0.776471, 0.666667, 0.203922,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.262745, 0.447059, 0.282353, 0.447059, 0.639216, 0.890196,
                        0.996078, 0.882353, 0.996078, 0.996078, 0.996078, 0.980392,
                        0.898039, 0.996078, 0.996078, 0.549020, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.066667, 0.258824, 0.054902,
                        0.262745, 0.262745, 0.262745, 0.231373, 0.082353, 0.925490,
                        0.996078, 0.415686, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.325490, 0.992157, 0.819608, 0.070588,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.086275,
                        0.913725, 1.000000, 0.325490, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.505882, 0.996078, 0.933333,
                        0.172549, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.231373, 0.976471, 0.996078, 0.243137, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.521569, 0.996078,
                        0.733333, 0.019608, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.035294, 0.803922, 0.972549, 0.227451, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.494118,
                        0.996078, 0.713725, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.294118, 0.984314, 0.941176, 0.223529,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.074510,
                        0.866667, 0.996078, 0.650980, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.011765, 0.796078, 0.996078, 0.858824,
                        0.137255, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.149020, 0.996078, 0.996078, 0.301961, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.121569, 0.878431, 0.996078,
                        0.450980, 0.003922, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.521569, 0.996078, 0.996078, 0.203922, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.239216, 0.949020,
                        0.996078, 0.996078, 0.203922, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.474510, 0.996078, 0.996078, 0.858824,
                        0.156863, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.474510, 0.996078, 0.811765, 0.070588, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000};

  for (ai_size i=0;  i < AI_LESP_IN_1_SIZE; i++ ){
    if (type_ == AI_BUFFER_FMT_TYPE_FLOAT)
      ((ai_float *)in_data)[i] = val[i];
  }
  aiRun(in_data, out_data);
  int max_idx = 0;
  float max_value = 0;
  for(int i=0;i<10;i++){
    if(((ai_float *)out_data)[i] > max_value){
      max_value = ((ai_float *)out_data)[i] ;
      max_idx = i;
    }
  }
  printf("%d",max_idx);
  printf("hello\n");
}

#ifdef __cplusplus
}
#endif
