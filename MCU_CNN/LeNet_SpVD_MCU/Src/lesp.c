/**
  ******************************************************************************
  * @file    lesp.c
  * @author  AST Embedded Analytics Research Platform
  * @date    Sat Mar  7 14:51:15 2020
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */


#include "lesp.h"

#include "ai_platform_interface.h"
#include "ai_math_helpers.h"

#include "core_common.h"
#include "layers.h"

#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#define AI_TOOLS_VERSION_MAJOR 5
#define AI_TOOLS_VERSION_MINOR 0
#define AI_TOOLS_VERSION_MICRO 0


#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#define AI_TOOLS_API_VERSION_MAJOR 1
#define AI_TOOLS_API_VERSION_MINOR 3
#define AI_TOOLS_API_VERSION_MICRO 0

#undef AI_NET_OBJ_INSTANCE
#define AI_NET_OBJ_INSTANCE g_lesp
 
#undef AI_LESP_MODEL_SIGNATURE
#define AI_LESP_MODEL_SIGNATURE     "6a4f0b719c05d151f94073c30d64ec8d"

#ifndef AI_TOOLS_REVISION_ID
#define AI_TOOLS_REVISION_ID     "(rev-5.0.0)"
#endif

#undef AI_TOOLS_DATE_TIME
#define AI_TOOLS_DATE_TIME   "Sat Mar  7 14:51:15 2020"

#undef AI_TOOLS_COMPILE_TIME
#define AI_TOOLS_COMPILE_TIME    __DATE__ " " __TIME__

#undef AI_LESP_N_BATCHES
#define AI_LESP_N_BATCHES         (1)

/**  Forward network declaration section  *************************************/
AI_STATIC ai_network AI_NET_OBJ_INSTANCE;


/**  Forward network array declarations  **************************************/
AI_STATIC ai_array dense_weights_array;   /* Array #0 */
AI_STATIC ai_array conv2d_7_bias_array;   /* Array #1 */
AI_STATIC ai_array conv2d_7_weights_array;   /* Array #2 */
AI_STATIC ai_array conv2d_6_bias_array;   /* Array #3 */
AI_STATIC ai_array conv2d_6_weights_array;   /* Array #4 */
AI_STATIC ai_array conv2d_5_bias_array;   /* Array #5 */
AI_STATIC ai_array conv2d_5_weights_array;   /* Array #6 */
AI_STATIC ai_array conv2d_4_bias_array;   /* Array #7 */
AI_STATIC ai_array conv2d_4_weights_array;   /* Array #8 */
AI_STATIC ai_array conv2d_3_bias_array;   /* Array #9 */
AI_STATIC ai_array conv2d_3_weights_array;   /* Array #10 */
AI_STATIC ai_array conv2d_2_bias_array;   /* Array #11 */
AI_STATIC ai_array conv2d_2_weights_array;   /* Array #12 */
AI_STATIC ai_array conv2d_1_bias_array;   /* Array #13 */
AI_STATIC ai_array conv2d_1_weights_array;   /* Array #14 */
AI_STATIC ai_array conv2d_bias_array;   /* Array #15 */
AI_STATIC ai_array conv2d_weights_array;   /* Array #16 */
AI_STATIC ai_array input_0_output_array;   /* Array #17 */
AI_STATIC ai_array conv2d_output_array;   /* Array #18 */
AI_STATIC ai_array conv2d_1_output_array;   /* Array #19 */
AI_STATIC ai_array conv2d_2_output_array;   /* Array #20 */
AI_STATIC ai_array conv2d_3_output_array;   /* Array #21 */
AI_STATIC ai_array conv2d_4_output_array;   /* Array #22 */
AI_STATIC ai_array conv2d_5_output_array;   /* Array #23 */
AI_STATIC ai_array conv2d_6_output_array;   /* Array #24 */
AI_STATIC ai_array conv2d_7_output_array;   /* Array #25 */
AI_STATIC ai_array dense_output_array;   /* Array #26 */
AI_STATIC ai_array dense_nl_output_array;   /* Array #27 */


/**  Forward network tensor declarations  *************************************/
AI_STATIC ai_tensor dense_weights;   /* Tensor #0 */
AI_STATIC ai_tensor conv2d_7_bias;   /* Tensor #1 */
AI_STATIC ai_tensor conv2d_7_weights;   /* Tensor #2 */
AI_STATIC ai_tensor conv2d_6_bias;   /* Tensor #3 */
AI_STATIC ai_tensor conv2d_6_weights;   /* Tensor #4 */
AI_STATIC ai_tensor conv2d_5_bias;   /* Tensor #5 */
AI_STATIC ai_tensor conv2d_5_weights;   /* Tensor #6 */
AI_STATIC ai_tensor conv2d_4_bias;   /* Tensor #7 */
AI_STATIC ai_tensor conv2d_4_weights;   /* Tensor #8 */
AI_STATIC ai_tensor conv2d_3_bias;   /* Tensor #9 */
AI_STATIC ai_tensor conv2d_3_weights;   /* Tensor #10 */
AI_STATIC ai_tensor conv2d_2_bias;   /* Tensor #11 */
AI_STATIC ai_tensor conv2d_2_weights;   /* Tensor #12 */
AI_STATIC ai_tensor conv2d_1_bias;   /* Tensor #13 */
AI_STATIC ai_tensor conv2d_1_weights;   /* Tensor #14 */
AI_STATIC ai_tensor conv2d_bias;   /* Tensor #15 */
AI_STATIC ai_tensor conv2d_weights;   /* Tensor #16 */
AI_STATIC ai_tensor input_0_output;   /* Tensor #17 */
AI_STATIC ai_tensor conv2d_output;   /* Tensor #18 */
AI_STATIC ai_tensor conv2d_1_output;   /* Tensor #19 */
AI_STATIC ai_tensor conv2d_2_output;   /* Tensor #20 */
AI_STATIC ai_tensor conv2d_3_output;   /* Tensor #21 */
AI_STATIC ai_tensor conv2d_4_output;   /* Tensor #22 */
AI_STATIC ai_tensor conv2d_5_output;   /* Tensor #23 */
AI_STATIC ai_tensor conv2d_6_output;   /* Tensor #24 */
AI_STATIC ai_tensor conv2d_7_output;   /* Tensor #25 */
AI_STATIC ai_tensor conv2d_7_output0;   /* Tensor #26 */
AI_STATIC ai_tensor dense_output;   /* Tensor #27 */
AI_STATIC ai_tensor dense_nl_output;   /* Tensor #28 */


/**  Forward network tensor chain declarations  *******************************/
AI_STATIC_CONST ai_tensor_chain conv2d_chain;   /* Chain #0 */
AI_STATIC_CONST ai_tensor_chain conv2d_1_chain;   /* Chain #1 */
AI_STATIC_CONST ai_tensor_chain conv2d_2_chain;   /* Chain #2 */
AI_STATIC_CONST ai_tensor_chain conv2d_3_chain;   /* Chain #3 */
AI_STATIC_CONST ai_tensor_chain conv2d_4_chain;   /* Chain #4 */
AI_STATIC_CONST ai_tensor_chain conv2d_5_chain;   /* Chain #5 */
AI_STATIC_CONST ai_tensor_chain conv2d_6_chain;   /* Chain #6 */
AI_STATIC_CONST ai_tensor_chain conv2d_7_chain;   /* Chain #7 */
AI_STATIC_CONST ai_tensor_chain dense_chain;   /* Chain #8 */
AI_STATIC_CONST ai_tensor_chain dense_nl_chain;   /* Chain #9 */


/**  Forward network layer declarations  **************************************/
AI_STATIC ai_layer_conv2d conv2d_layer; /* Layer #0 */
AI_STATIC ai_layer_conv2d conv2d_1_layer; /* Layer #1 */
AI_STATIC ai_layer_conv2d conv2d_2_layer; /* Layer #2 */
AI_STATIC ai_layer_conv2d conv2d_3_layer; /* Layer #3 */
AI_STATIC ai_layer_conv2d conv2d_4_layer; /* Layer #4 */
AI_STATIC ai_layer_conv2d conv2d_5_layer; /* Layer #5 */
AI_STATIC ai_layer_conv2d conv2d_6_layer; /* Layer #6 */
AI_STATIC ai_layer_conv2d conv2d_7_layer; /* Layer #7 */
AI_STATIC ai_layer_dense dense_layer; /* Layer #8 */
AI_STATIC ai_layer_nl dense_nl_layer; /* Layer #9 */


/**  Array declarations section  **********************************************/
AI_ARRAY_OBJ_DECLARE(
    dense_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 9800,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 20,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 4000,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_6_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 8,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_6_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 160,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 20,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 10000,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 20,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 3420,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 19,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 912,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 3,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 60,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_1_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 20,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_1_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 9500,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_bias_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 19,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_weights_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 475,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    input_0_output_array, AI_ARRAY_FORMAT_FLOAT|AI_FMT_FLAG_IS_IO,
    NULL, NULL, 784,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 10944,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_1_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 8000,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 1200,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 5491,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 4500,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 2420,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_6_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 968,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 980,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    dense_output_array, AI_ARRAY_FORMAT_FLOAT,
    NULL, NULL, 10,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    dense_nl_output_array, AI_ARRAY_FORMAT_FLOAT|AI_FMT_FLAG_IS_IO,
    NULL, NULL, 10,
     AI_STATIC)




/**  Tensor declarations section  *********************************************/
AI_TENSOR_OBJ_DECLARE(
  dense_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 980, 10, 1, 1), AI_STRIDE_INIT(4, 4, 3920, 39200, 39200),
  1, &dense_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 1, 1), AI_STRIDE_INIT(4, 4, 4, 80, 80),
  1, &conv2d_7_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 8, 5, 5, 20), AI_STRIDE_INIT(4, 4, 32, 160, 800),
  1, &conv2d_7_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_6_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 8, 1, 1), AI_STRIDE_INIT(4, 4, 4, 32, 32),
  1, &conv2d_6_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_6_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 20, 1, 1, 8), AI_STRIDE_INIT(4, 4, 80, 80, 80),
  1, &conv2d_6_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 1, 1), AI_STRIDE_INIT(4, 4, 4, 80, 80),
  1, &conv2d_5_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 20, 5, 5, 20), AI_STRIDE_INIT(4, 4, 80, 400, 2000),
  1, &conv2d_5_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 1, 1), AI_STRIDE_INIT(4, 4, 4, 80, 80),
  1, &conv2d_4_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 19, 3, 3, 20), AI_STRIDE_INIT(4, 4, 76, 228, 684),
  1, &conv2d_4_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 19, 1, 1), AI_STRIDE_INIT(4, 4, 4, 76, 76),
  1, &conv2d_3_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 3, 4, 4, 19), AI_STRIDE_INIT(4, 4, 12, 48, 192),
  1, &conv2d_3_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3, 1, 1), AI_STRIDE_INIT(4, 4, 4, 12, 12),
  1, &conv2d_2_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 20, 1, 1, 3), AI_STRIDE_INIT(4, 4, 80, 80, 80),
  1, &conv2d_2_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 1, 1), AI_STRIDE_INIT(4, 4, 4, 80, 80),
  1, &conv2d_1_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 19, 5, 5, 20), AI_STRIDE_INIT(4, 4, 76, 380, 1900),
  1, &conv2d_1_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 19, 1, 1), AI_STRIDE_INIT(4, 4, 4, 76, 76),
  1, &conv2d_bias_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 5, 5, 19), AI_STRIDE_INIT(4, 4, 4, 20, 100),
  1, &conv2d_weights_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  input_0_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1, 28, 28), AI_STRIDE_INIT(4, 4, 4, 4, 112),
  1, &input_0_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 19, 24, 24), AI_STRIDE_INIT(4, 4, 4, 76, 1824),
  1, &conv2d_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 20, 20), AI_STRIDE_INIT(4, 4, 4, 80, 1600),
  1, &conv2d_1_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3, 20, 20), AI_STRIDE_INIT(4, 4, 4, 12, 240),
  1, &conv2d_2_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 19, 17, 17), AI_STRIDE_INIT(4, 4, 4, 76, 1292),
  1, &conv2d_3_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 15, 15), AI_STRIDE_INIT(4, 4, 4, 80, 1200),
  1, &conv2d_4_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 11, 11), AI_STRIDE_INIT(4, 4, 4, 80, 880),
  1, &conv2d_5_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_6_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 8, 11, 11), AI_STRIDE_INIT(4, 4, 4, 32, 352),
  1, &conv2d_6_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 20, 7, 7), AI_STRIDE_INIT(4, 4, 4, 80, 560),
  1, &conv2d_7_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_output0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 980, 1, 1), AI_STRIDE_INIT(4, 4, 4, 3920, 3920),
  1, &conv2d_7_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  dense_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 10, 1, 1), AI_STRIDE_INIT(4, 4, 4, 40, 40),
  1, &dense_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  dense_nl_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 10, 1, 1), AI_STRIDE_INIT(4, 4, 4, 40, 40),
  1, &dense_nl_output_array, NULL)


/**  Layer declarations section  **********************************************/


AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&input_0_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_weights, &conv2d_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_layer, 0,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_1_layer, AI_STATIC,
  .tensors = &conv2d_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_1_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_1_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_1_weights, &conv2d_1_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_1_layer, 1,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_2_layer, AI_STATIC,
  .tensors = &conv2d_1_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_2_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_1_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_2_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_2_weights, &conv2d_2_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_2_layer, 2,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_3_layer, AI_STATIC,
  .tensors = &conv2d_2_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_3_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_2_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_3_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_3_weights, &conv2d_3_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_3_layer, 3,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_4_layer, AI_STATIC,
  .tensors = &conv2d_3_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_4_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_3_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_4_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_4_weights, &conv2d_4_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_4_layer, 4,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_5_layer, AI_STATIC,
  .tensors = &conv2d_4_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_5_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_4_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_5_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_5_weights, &conv2d_5_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_5_layer, 5,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_6_layer, AI_STATIC,
  .tensors = &conv2d_5_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_6_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_5_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_6_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_6_weights, &conv2d_6_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_6_layer, 6,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &conv2d_7_layer, AI_STATIC,
  .tensors = &conv2d_6_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_7_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_6_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_7_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_7_weights, &conv2d_7_bias, NULL),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conv2d_7_layer, 7,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &dense_layer, AI_STATIC,
  .tensors = &conv2d_7_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  dense_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_7_output0),
  AI_TENSOR_LIST_ENTRY(&dense_output),
  AI_TENSOR_LIST_ENTRY(&dense_weights),
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  dense_layer, 9,
  DENSE_TYPE,
  dense, forward_dense,
  &AI_NET_OBJ_INSTANCE, &dense_nl_layer, AI_STATIC,
  .tensors = &dense_chain, 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  dense_nl_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&dense_output),
  AI_TENSOR_LIST_ENTRY(&dense_nl_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  dense_nl_layer, 9,
  NL_TYPE,
  nl, forward_sm,
  &AI_NET_OBJ_INSTANCE, &dense_nl_layer, AI_STATIC,
  .tensors = &dense_nl_chain, 
)


AI_NETWORK_OBJ_DECLARE(
  AI_NET_OBJ_INSTANCE, AI_STATIC,
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 153824, 1,
                     NULL),
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 45456, 1,
                     NULL),
  AI_TENSOR_LIST_IO_ENTRY(AI_FLAG_NONE, AI_LESP_IN_NUM, &input_0_output),
  AI_TENSOR_LIST_IO_ENTRY(AI_FLAG_NONE, AI_LESP_OUT_NUM, &dense_nl_output),
  &conv2d_layer, 0, NULL)



AI_DECLARE_STATIC
ai_bool lesp_configure_activations(
  ai_network* net_ctx, const ai_buffer* activation_buffer)
{
  AI_ASSERT(net_ctx &&  activation_buffer && activation_buffer->data)

  ai_ptr activations = AI_PTR(AI_PTR_ALIGN(activation_buffer->data, 4));
  AI_ASSERT(activations)
  AI_UNUSED(net_ctx)

  {
    /* Updating activations (byte) offsets */
    input_0_output_array.data = AI_PTR(NULL);
    input_0_output_array.data_start = AI_PTR(NULL);
    conv2d_output_array.data = AI_PTR(activations + 1680);
    conv2d_output_array.data_start = AI_PTR(activations + 1680);
    conv2d_1_output_array.data = AI_PTR(activations + 0);
    conv2d_1_output_array.data_start = AI_PTR(activations + 0);
    conv2d_2_output_array.data = AI_PTR(activations + 40656);
    conv2d_2_output_array.data_start = AI_PTR(activations + 40656);
    conv2d_3_output_array.data = AI_PTR(activations + 22124);
    conv2d_3_output_array.data_start = AI_PTR(activations + 22124);
    conv2d_4_output_array.data = AI_PTR(activations + 20844);
    conv2d_4_output_array.data_start = AI_PTR(activations + 20844);
    conv2d_5_output_array.data = AI_PTR(activations + 19884);
    conv2d_5_output_array.data_start = AI_PTR(activations + 19884);
    conv2d_6_output_array.data = AI_PTR(activations + 19500);
    conv2d_6_output_array.data_start = AI_PTR(activations + 19500);
    conv2d_7_output_array.data = AI_PTR(activations + 18812);
    conv2d_7_output_array.data_start = AI_PTR(activations + 18812);
    dense_output_array.data = AI_PTR(activations + 18772);
    dense_output_array.data_start = AI_PTR(activations + 18772);
    dense_nl_output_array.data = AI_PTR(NULL);
    dense_nl_output_array.data_start = AI_PTR(NULL);
    
  }
  return true;
}



AI_DECLARE_STATIC
ai_bool lesp_configure_weights(
  ai_network* net_ctx, const ai_buffer* weights_buffer)
{
  AI_ASSERT(net_ctx &&  weights_buffer && weights_buffer->data)

  ai_ptr weights = AI_PTR(weights_buffer->data);
  AI_ASSERT(weights)
  AI_UNUSED(net_ctx)

  {
    /* Updating weights (byte) offsets */
    
    dense_weights_array.format |= AI_FMT_FLAG_CONST;
    dense_weights_array.data = AI_PTR(weights + 114624);
    dense_weights_array.data_start = AI_PTR(weights + 114624);
    conv2d_7_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_7_bias_array.data = AI_PTR(weights + 114544);
    conv2d_7_bias_array.data_start = AI_PTR(weights + 114544);
    conv2d_7_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_7_weights_array.data = AI_PTR(weights + 98544);
    conv2d_7_weights_array.data_start = AI_PTR(weights + 98544);
    conv2d_6_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_6_bias_array.data = AI_PTR(weights + 98512);
    conv2d_6_bias_array.data_start = AI_PTR(weights + 98512);
    conv2d_6_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_6_weights_array.data = AI_PTR(weights + 97872);
    conv2d_6_weights_array.data_start = AI_PTR(weights + 97872);
    conv2d_5_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_5_bias_array.data = AI_PTR(weights + 97792);
    conv2d_5_bias_array.data_start = AI_PTR(weights + 97792);
    conv2d_5_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_5_weights_array.data = AI_PTR(weights + 57792);
    conv2d_5_weights_array.data_start = AI_PTR(weights + 57792);
    conv2d_4_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_4_bias_array.data = AI_PTR(weights + 57712);
    conv2d_4_bias_array.data_start = AI_PTR(weights + 57712);
    conv2d_4_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_4_weights_array.data = AI_PTR(weights + 44032);
    conv2d_4_weights_array.data_start = AI_PTR(weights + 44032);
    conv2d_3_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_3_bias_array.data = AI_PTR(weights + 43956);
    conv2d_3_bias_array.data_start = AI_PTR(weights + 43956);
    conv2d_3_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_3_weights_array.data = AI_PTR(weights + 40308);
    conv2d_3_weights_array.data_start = AI_PTR(weights + 40308);
    conv2d_2_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_2_bias_array.data = AI_PTR(weights + 40296);
    conv2d_2_bias_array.data_start = AI_PTR(weights + 40296);
    conv2d_2_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_2_weights_array.data = AI_PTR(weights + 40056);
    conv2d_2_weights_array.data_start = AI_PTR(weights + 40056);
    conv2d_1_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_1_bias_array.data = AI_PTR(weights + 39976);
    conv2d_1_bias_array.data_start = AI_PTR(weights + 39976);
    conv2d_1_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_1_weights_array.data = AI_PTR(weights + 1976);
    conv2d_1_weights_array.data_start = AI_PTR(weights + 1976);
    conv2d_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_bias_array.data = AI_PTR(weights + 1900);
    conv2d_bias_array.data_start = AI_PTR(weights + 1900);
    conv2d_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_weights_array.data = AI_PTR(weights + 0);
    conv2d_weights_array.data_start = AI_PTR(weights + 0);
  }

  return true;
}


/**  PUBLIC APIs SECTION  *****************************************************/

AI_API_ENTRY
ai_bool ai_lesp_get_info(
  ai_handle network, ai_network_report* report)
{
  ai_network* net_ctx = AI_NETWORK_ACQUIRE_CTX(network);

  if ( report && net_ctx )
  {
    ai_network_report r = {
      .model_name        = AI_LESP_MODEL_NAME,
      .model_signature   = AI_LESP_MODEL_SIGNATURE,
      .model_datetime    = AI_TOOLS_DATE_TIME,
      
      .compile_datetime  = AI_TOOLS_COMPILE_TIME,
      
      .runtime_revision  = ai_platform_runtime_get_revision(),
      .runtime_version   = ai_platform_runtime_get_version(),

      .tool_revision     = AI_TOOLS_REVISION_ID,
      .tool_version      = {AI_TOOLS_VERSION_MAJOR, AI_TOOLS_VERSION_MINOR,
                            AI_TOOLS_VERSION_MICRO, 0x0},
      .tool_api_version  = {AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR,
                            AI_TOOLS_API_VERSION_MICRO, 0x0},

      .api_version            = ai_platform_api_get_version(),
      .interface_api_version  = ai_platform_interface_api_get_version(),
      
      .n_macc            = 6598313,
      .n_inputs          = 0,
      .inputs            = NULL,
      .n_outputs         = 0,
      .outputs           = NULL,
      .activations       = AI_STRUCT_INIT,
      .params            = AI_STRUCT_INIT,
      .n_nodes           = 0,
      .signature         = 0x0,
    };

    if ( !ai_platform_api_get_network_report(network, &r) ) return false;

    *report = r;
    return true;
  }

  return false;
}

AI_API_ENTRY
ai_error ai_lesp_get_error(ai_handle network)
{
  return ai_platform_network_get_error(network);
}

AI_API_ENTRY
ai_error ai_lesp_create(
  ai_handle* network, const ai_buffer* network_config)
{
  return ai_platform_network_create(
    network, network_config, 
    &AI_NET_OBJ_INSTANCE,
    AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR, AI_TOOLS_API_VERSION_MICRO);
}

AI_API_ENTRY
ai_handle ai_lesp_destroy(ai_handle network)
{
  return ai_platform_network_destroy(network);
}

AI_API_ENTRY
ai_bool ai_lesp_init(
  ai_handle network, const ai_network_params* params)
{
  ai_network* net_ctx = ai_platform_network_init(network, params);
  if ( !net_ctx ) return false;

  ai_bool ok = true;
  ok &= lesp_configure_weights(net_ctx, &params->params);
  ok &= lesp_configure_activations(net_ctx, &params->activations);

  return ok;
}


AI_API_ENTRY
ai_i32 ai_lesp_run(
  ai_handle network, const ai_buffer* input, ai_buffer* output)
{
  return ai_platform_network_process(network, input, output);
}

AI_API_ENTRY
ai_i32 ai_lesp_forward(ai_handle network, const ai_buffer* input)
{
  return ai_platform_network_process(network, input, NULL);
}

#undef AI_LESP_MODEL_SIGNATURE
#undef AI_NET_OBJ_INSTANCE
#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#undef AI_TOOLS_DATE_TIME
#undef AI_TOOLS_COMPILE_TIME

